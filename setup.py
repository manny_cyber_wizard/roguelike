import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="roguelike",
    version="0.0.1",
    author="manny",
    author_email="manny@cyber-wizard.com",
    description="A roguelike game",
    license="GPL version 3",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/manny_cyber_wizard/bletchley",
    keywords = "game roguelike",
    packages=setuptools.find_packages(),
    zip_safe=False,
    install_requires=[
        "bearlibterminal>=0.15.7",
    ],
    classifiers=[
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3 :: Only",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
        "Operating System :: OS Independent",
        "Development Status :: 3 - Alpha",
    ],
    project_urls={ 
        'Bug Reports': 'https://gitlab.com/manny_cyber_wizard/roguelike/issues',
        'Say Thanks!': 'https://saythanks.io/to/MannyCyber',
        'Source': 'https://gitlab.com/manny_cyber_wizard/roguelike',
    },
)
